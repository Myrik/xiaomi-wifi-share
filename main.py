import cv2
from pyzbar import pyzbar
import numpy
from xiaomi_wifi import nm_save, XiaomiWifi
from typing import Optional


class CV2Window:
    def __init__(self, name):
        self.name = name

    def __enter__(self):
        cv2.namedWindow(self.name)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        cv2.destroyWindow(self.name)
        if exc_val:
            raise

    def imshow(self, image):
        cv2.imshow(self.name, image)
        cv2.waitKey(1)


class CV2Camera:
    cam = None

    def __init__(self, dev_num=0):
        self.dev_num = dev_num

    def __enter__(self):
        self.cam = cv2.VideoCapture(self.dev_num)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.cam.release()
        if exc_val:
            raise

    def image(self):
        retval = False
        while not retval:
            retval, image = self.cam.read()

        return image


def decode(image) -> Optional[str]:
    decoded_objects = pyzbar.decode(image)
    if decoded_objects:
        decoded_object = decoded_objects[0]
        return decoded_object.data.decode()


def get_qr(dev_num=0) -> str:
    with CV2Camera(dev_num) as cam:
        decoded = None
        with CV2Window("Camera") as window:
            while not decoded:
                image = cam.image()
                window.imshow(image)
                decoded = decode(image)

        return decoded


if __name__ == "__main__":
    data = get_qr()
    try:
        wifi = XiaomiWifi(data)
    except:
        raise Exception("Unsupported qr format")
    nm_save(wifi)
