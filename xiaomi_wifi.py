from dataclasses import dataclass

import os


@dataclass
class XiaomiWifi:
    name: str
    password: str
    security: str

    def __init__(self, data: str):
        if not data.startswith("WIFI:"):
            raise Exception(f"Invalid data: {data}")

        items = data.replace("WIFI:", "").split(";")
        items = dict(i.split(":") for i in items if i)

        self.name=items["S"]
        self.password=items["P"]
        self.security=items["T"]


def nm_save(data: XiaomiWifi):
    cmd = f'nmcli device wifi con "{data.name}" password "{data.password}"'
    output = os.popen(cmd).read()
    print(output)
